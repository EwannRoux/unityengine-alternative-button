﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.UI;
#endif
public class AlternativeButton_Core : Selectable, ISubmitHandler, IPointerUpHandler, IPointerDownHandler, IDragHandler, IBeginDragHandler, IEndDragHandler {

    protected override void Start() {
        base.Start();

        //Only a draggable button need a valid parent ScrollRect
        if (isDraggableButton) {
            //Set _hasValidScrollRect as true of false.
            CheckIfParentScrollRectIsValid(_parentScrollRect);
        }
       
    }


    #region VARIABLES

    [SerializeField]
    Canvas _canvas;

    [SerializeField]
    bool _checkIfCursorIsHover;

    [SerializeField]
    public bool TriggerOnClickAtPointerUp;

    [SerializeField, Tooltip("Treat gamepad submit like a click from the mouse ?")]
    private bool _computeSubmitAsClick;

    /// <summary>
    /// Used in the OnPointerUp() method to determine if the button must be considered as triggered. Set in the OnPointeEnter and OnPointerExit() methods.
    /// </summary>
    public bool CursorIsHovering { get; private set; }

    #endregion


    #region DRAGGABLE_BUTTON_VARIABLES

    [SerializeField]
    public bool isDraggableButton = false;
    [SerializeField]
    public float maxDragDistanceForInvokingOnClick = 15f;

    //Not SerializeField bc only evaluated and used at runtime. Protected to use it in inheriting classes.
    protected Vector2 buttonPositionAtStartOfDrag;

    //Not SerializeField bc only evaluated and used at runtime.
    private bool _hasValidScrollRect;

    [SerializeField]
    private ScrollRect _parentScrollRect;
    public ScrollRect ParentScrollRect {
        get {
            return _parentScrollRect;
        }
        set {
            _parentScrollRect = value;
            CheckIfParentScrollRectIsValid(value);
        }
    }

    #endregion DRAGGABLE_BUTTON_VARIABLES


    #region DRAGGABLE_BUTTON_METHODS

    protected void CheckIfParentScrollRectIsValid(ScrollRect scrollRect) {
        //scrollRect need to be non-null and (grand)parent of the transform to be valid.
        if (scrollRect != null) {
            if (transform.IsChildOrSubChildOf(_parentScrollRect.transform)) {
                _hasValidScrollRect = true;
            } else {
                _hasValidScrollRect = false;
            }
        } else {
            _hasValidScrollRect = false;
        }
    }

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData) {
        //The eventData must be redirected only if this is a draggable button but it is better to
        //check if it has a scroll rect to prevent any NullReferenceException to happen.
        //The redirected event allow the scroll rect to work properly, since this button intercept it.
        if (isDraggableButton && _hasValidScrollRect) {
            _parentScrollRect.OnBeginDrag(eventData);
        }
        OnBeginDrag_AdditionalExecution(eventData);
    }

    /// <summary>
    /// Called by IBeginDragHandler.OnBeginDrag, meant to be used in class inheriting EnhancedButton_Core.
    /// </summary>
    /// <param name="eventData">PointerEventData received from the IBeginDragHandler.OnBeginDrag</param>
    protected virtual void OnBeginDrag_AdditionalExecution(PointerEventData eventData) {}


    void IEndDragHandler.OnEndDrag(PointerEventData eventData) {
        //The eventData must be redirected only if this is a draggable button but it is better to
        //check if it has a scroll rect to prevent any NullReferenceException to happen.
        //The redirected event allow the scroll rect to work properly, since this button intercept it.
        if (_hasValidScrollRect) {
            _parentScrollRect.OnEndDrag(eventData);
        }
    }

    /// <summary>
    /// Called by IEndDragHandler.OnEndDrag, meant to be used in class inheriting EnhancedButton_Core.
    /// </summary>
    /// <param name="eventData">PointerEventData received from the IEndDragHandler.OnEndDrag(</param>
    protected virtual void OnEndDrag_AdditionalExecution(PointerEventData eventData) { }


    void IDragHandler.OnDrag(PointerEventData eventData) {
        //The eventData must be redirected only if this is a draggable button but it is better to
        //check if it has a scroll rect to prevent any NullReferenceException to happen.
        //The redirected event allow the scroll rect to work properly, since this button intercept it.
        if (_hasValidScrollRect) {
            _parentScrollRect.OnDrag(eventData);
        }
    }

    /// <summary>
    /// Called by IDragHandler.OnDrag, meant to be used in class inheriting EnhancedButton_Core.
    /// </summary>
    /// <param name="eventData">PointerEventData received from the IDragHandler.OnDrag</param>
    protected virtual void OnDrag_AdditionalExecution(PointerEventData eventData) { }

    #endregion



    /// <summary>
    /// Invoked when pressing the input assigned as "Submit" in the Input.
    /// </summary>
    void ISubmitHandler.OnSubmit(BaseEventData eventData) {
        //If button is not interactable, do nothing.
        if (!interactable) {
            return;
        }
        //If compute "Submit" as "Click", invoke OnClick...
        if (_computeSubmitAsClick) {
            OnClick.Invoke();
        } else {
            //else, invoke the OnSubmit UnityEvent.
            OnSubmit.Invoke();
        }
    }

    /// <summary>
    /// Invoked when a pointer (like the mouse primary button) is pressed hover this button.
    /// </summary>
    public override void OnPointerDown(PointerEventData eventData) {
        //If button is not interactable, do nothing.
        if (!interactable) {
            return;
        }
        //If this button is not draggable, but a default button instead:
        if (isDraggableButton == false) {

            //If TriggerClickAtPointerUp is false, then the button is treated as
            //triggered when pointer is down
            if (TriggerOnClickAtPointerUp == false) {
                //Call method base.
                base.OnPointerDown(eventData);
                //Invoke the OnClick UnityEvent.
                OnClick.Invoke();
                //Return now, bc the last part of the method is used both by draggable
                //button and when TriggerClickAtPointerUp is true
                return;
            }
        }

        //If this is a draggable button, record its transform position,
        //instead of treading the buttn as clicked. 
        if (isDraggableButton) {
            buttonPositionAtStartOfDrag = transform.GetComponent<RectTransform>().position;
        }
    }

    /// <summary>
    /// Invoked when a pointer (like the mouse primary button) is released and this button was the last target of OnPointerDown.
    /// So even if the pointer was moved out of the button, this will be called on release.
    /// Reciprocally, even if a pointer is released on top of this button, if OnPointerDown was not invoked previously, this 
    /// method will not be invoked in this instance.
    /// </summary>
    public override void OnPointerUp(PointerEventData eventData) {
        //If button is not interactable, do nothing.
        if (!interactable) {
            return;
        }

        //Called before any other return-check, bc it is necessary to update the currrent state and display of the button.
        base.OnPointerUp(eventData);
       
        //If cursor was moved out of button, do nothing.
        if (CursorIsHovering == false) {
            Debug.Log("cursor not hover, returning");
            return;
        }

        //If it is a draggable button, 
        if (isDraggableButton) {
            //calculate the distance between start and end of drag
            float distanceFromDrag = Vector2.Distance(buttonPositionAtStartOfDrag, transform.GetComponent<RectTransform>().position);
#if UNITY_EDITOR
            //Display the drag distance in console, to help estimate the value considered as acceptable when editing in the inspector.
            Debug.LogFormat("Distance drag is {0}", distanceFromDrag);
#endif
            //If drag was negligible, invoke the UnityEvent.
            if (distanceFromDrag < maxDragDistanceForInvokingOnClick) {
                OnClick.Invoke();
#if UNITY_EDITOR
                //To help determine if OnClick was invoked on very fast UI set-up. 
                Debug.Log("Draggable button invoked OnClick()");
#endif
            }
#if UNITY_EDITOR
            else {
                //To help determine if OnClick was invoked on very fast UI set-up. 
                Debug.LogFormat("Daggable button DOESN'T invoke OnClick(), dragged distance is {0}, with a threshold of {1}", distanceFromDrag, maxDragDistanceForInvokingOnClick);
            }
#endif

        }
        else if (TriggerOnClickAtPointerUp) { 
            OnClick.Invoke();
        }
    }

    /// <summary>
    /// Invoked when the event system input navigation set this selectable as the currently selected Object.
    /// </summary>
    public override void OnSelect(BaseEventData eventData) {
        //If button is not interactable, do nothing.
        //Normally the OnSelect method will not be called if the Selectable.interactable is set as false,
        //but it's better to check this just in case. And it's also possible that another script try to
        //directly call OnSelect. 
        if (!interactable) {
            return;
        }
        base.OnSelect(eventData);
        //Invoke the UnityEvent
        OnSelectByNavigation.Invoke();
    }

    /// <summary>
    /// Invoked when the event system input navigation set another Object as the currently selected Object, and this was the previous selected Object..
    /// </summary>
    public override void OnDeselect(BaseEventData eventData) {
        //If button is not interactable, do nothing.
        //Normally the OnDeselect method will not be called if the Selectable.interacle is set as false,
        //but it's better to check this just in case. And it's also possible that another script try to
        //directly call OnDeselect.
        if (!interactable) {
        return;
        }
        base.OnDeselect(eventData);
        //Invoke the UnityEvent
        OnDeselectByNavigation.Invoke();
    }
    protected override void OnEnable() {
        base.OnEnable();


        if (_checkIfCursorIsHover) {
            CheckIfCursorIsHovering();
        }
        else {
            CursorIsHovering = false;
        }






    }

    protected override void OnDisable() {
        base.OnDisable();
    }



  




    /// <summary>
    /// Immdiatly check if cursor is hovering the rect. This method isn't very optimized or fast, so checking if cursor is hovering is usefull only when high attention to detail on display is required.
    /// </summary>
    private void CheckIfCursorIsHovering() {


        if (_canvas) {

            if (transform.IsChildOf(_canvas.transform) == false) {
                return;
            }


            Debug.Log("in canvas check " + name + " " + Time.frameCount, gameObject);
            if (_canvas.renderMode != RenderMode.WorldSpace) {


                Debug.Log("in not world space  check");
                Vector2 mousePosition = Mouse.current.position.ReadValue();

                bool contains = false;

                //A ScreenSpaceCamera with no camera is treated as an ScreenSpaceOverlay canvas.
                if (_canvas.renderMode == RenderMode.ScreenSpaceOverlay || _canvas.worldCamera == null) {
                    contains = RectTransformUtility.RectangleContainsScreenPoint((RectTransform)transform, mousePosition);

                    Debug.Log("in overlay check " + contains);
                }

                if (_canvas.renderMode == RenderMode.ScreenSpaceCamera) {
                    contains = RectTransformUtility.RectangleContainsScreenPoint((RectTransform)transform, mousePosition, _canvas.worldCamera);
                }


                if (contains) {
                    FakeOnPointerEnter();
                }
            }
        }
    }

  





    /// <summary>
    /// Invoked when the pointer enter the rect of the button.
    /// </summary>
    public override void OnPointerEnter(PointerEventData eventData) {
        //If button is not interactable, do nothing.
        if (!interactable) {
            return;
        }
        //Call base method to trigger state update.
        base.OnPointerEnter(eventData);

        //Invoke the UnityEvent
        OnStartHighlight.Invoke();

        //Set this variable as true, it will be used in OnPointerUp();
        CursorIsHovering = true;

        //If button is navigable, set it as selected game object, so, in case of switch from mouse to
        //gamepad, the navigation will pursue from this button instead of the default selectable of the Event System.
        if (navigation.mode != Navigation.Mode.None) {
            EventSystem.current.SetSelectedGameObject(this.gameObject);
        }


    }
  
    /// <summary>
    /// Called during OnEnable if the mouse is over the button.
    /// </summary>
    public  void FakeOnPointerEnter() {
        //If button is not interactable, do nothing.
        if (!interactable) {
            return;
        }
        
        //Invoke the UnityEvent
        OnStartHighlight.Invoke();

        //Set this variable as true, it will be used in OnPointerUp();
        CursorIsHovering = true;

        DoStateTransition(SelectionState.Highlighted, true); 

        //If button is navigable, set it as selected game object, so, in case of switch from mouse to
        //gamepad, the navigation will pursue from this button instead of the default selectable of the Event System.
        if (navigation.mode != Navigation.Mode.None) {
            EventSystem.current.SetSelectedGameObject(this.gameObject);
        }


    }


    /// <summary>
    /// Invoked when the pointer exit the rect of the button.
    /// </summary>
    public override void OnPointerExit(PointerEventData eventData) {
        //If button is not interactable, do nothing.
        if (!interactable) {
            return;
        }
        //Call base method to trigger state update.
        base.OnPointerExit(eventData);

        //Invoke the UnityEvent
        OnStopHighlight.Invoke();

        //Set this variable as false, it will be used in OnPointerUp();
        CursorIsHovering = false;

        //If button is navigable and considered as selecteed game object, set selected game object as null, so, in case of switch from mouse to
        //gamepad, the navigation will instead use  the default selectable of the Event System.
        if (navigation.mode != Navigation.Mode.None) {
            if (EventSystem.current.currentSelectedGameObject == this.gameObject) {
                EventSystem.current.SetSelectedGameObject(null);
            }
        }
    }



    public UnityEvent OnClick;
    public UnityEvent OnSubmit;
    public UnityEvent OnStartHighlight;
    public UnityEvent OnStopHighlight;
    public UnityEvent OnSelectByNavigation;
    public UnityEvent OnDeselectByNavigation;


#if UNITY_EDITOR
    //Set the editor class in the same file so it can access privates variables of EnhancedButton_Core
    //Since this is editor script, we must use conditional compilation.

    [CustomEditor(typeof(AlternativeButton_Core)), CanEditMultipleObjects]
    public class AlternativeButton_Core_Editor : SelectableEditor {
        AlternativeButton_Core TargetCasted;
        protected override void OnEnable() {
            base.OnEnable();

            TargetCasted = (AlternativeButton_Core)target;

            OnClickSerialized = serializedObject.FindProperty(nameof(TargetCasted.OnClick));
            OnSubmitSerialized = serializedObject.FindProperty(nameof(TargetCasted.OnSubmit));

            OnStartHighlightSerialized = serializedObject.FindProperty(nameof(TargetCasted.OnStartHighlight));
            OnStopHighlightSerialized = serializedObject.FindProperty(nameof(TargetCasted.OnStopHighlight));
            OnSelectByNavigationSerialized = serializedObject.FindProperty(nameof(TargetCasted.OnSelectByNavigation));
            OnDeselectByNavigationSerialized = serializedObject.FindProperty(nameof(TargetCasted.OnDeselectByNavigation));


            ComputeSubmitAsClickSerialized = serializedObject.FindProperty(nameof(TargetCasted._computeSubmitAsClick));
            IsDraggableButtonSerialized = serializedObject.FindProperty(nameof(TargetCasted.isDraggableButton));
            ParentScrollRectSerialized = serializedObject.FindProperty(nameof(TargetCasted._parentScrollRect));
            DragThresoldSerialized = serializedObject.FindProperty(nameof(TargetCasted.maxDragDistanceForInvokingOnClick));
            TriggerClickAtPointerUpSerialized = serializedObject.FindProperty(nameof(TargetCasted.TriggerOnClickAtPointerUp));
            ParentCanvasSerialized = serializedObject.FindProperty(nameof(TargetCasted._canvas));
            CheckIfCursorIsHoverSerialized = serializedObject.FindProperty(nameof(TargetCasted._checkIfCursorIsHover));
        }

        public SerializedProperty OnClickSerialized;
        public SerializedProperty OnSubmitSerialized;
        public SerializedProperty OnStartHighlightSerialized;
        public SerializedProperty OnStopHighlightSerialized;
        public SerializedProperty OnSelectByNavigationSerialized;
        public SerializedProperty OnDeselectByNavigationSerialized;

        public SerializedProperty ParentScrollRectSerialized;

        public SerializedProperty ComputeSubmitAsClickSerialized;
        public SerializedProperty IsDraggableButtonSerialized;
        public SerializedProperty DragThresoldSerialized;

        public SerializedProperty TriggerClickAtPointerUpSerialized;

        public SerializedProperty ParentCanvasSerialized;
        public SerializedProperty CheckIfCursorIsHoverSerialized;

        public override void OnInspectorGUI() {
            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour((AlternativeButton_Core)target), typeof(AlternativeButton_Core), false);
            GUI.enabled = true;

            base.OnInspectorGUI();

            serializedObject.UpdateIfRequiredOrScript();

            EditorGUILayout.PropertyField(CheckIfCursorIsHoverSerialized, new GUIContent("Check if cursor is hover"), GUILayout.ExpandWidth(true));

            if(CheckIfCursorIsHoverSerialized.boolValue == true) {
                EditorGUILayout.PropertyField(ParentCanvasSerialized, new GUIContent("Parent Canvas"), GUILayout.ExpandWidth(true));

                bool displayError = true;

                if(ParentCanvasSerialized.objectReferenceValue != null) {
                    if (TargetCasted.transform.IsChildOf(((Canvas)ParentCanvasSerialized.objectReferenceValue).transform)) {
                        displayError = false;
                    }
                }

                if (displayError) {
                    EditorGUILayout.HelpBox("A Canvas reference which is a parent of this object is required"
                   , MessageType.Error);
                }
                EditorGUILayout.HelpBox("This function can be pretty heavy, only use on button that are susceptible to be highlighted when displayed. Use only if you really care about display, " +
               "since moving the cursor a few pixel will trigger correct display"
                  , MessageType.Warning);
            }
           




            EditorGUILayout.PropertyField(IsDraggableButtonSerialized, new GUIContent("Is Draggable Button"), GUILayout.ExpandWidth(true));

            if (IsDraggableButtonSerialized.boolValue) {


                EditorGUILayout.PropertyField(DragThresoldSerialized, new GUIContent("Max drag distance"), GUILayout.ExpandWidth(true));
                EditorGUILayout.HelpBox("Drag distance considered as negligible. Any drag further this distance will prevent OnClick to trigger."
                    , MessageType.None);

                if (DragThresoldSerialized.floatValue < 1f) {
                    DragThresoldSerialized.floatValue = 1f;
                }

                EditorGUILayout.Space();
                EditorGUILayout.PropertyField(ParentScrollRectSerialized, new GUIContent("Parent ScrollRect"), GUILayout.ExpandWidth(true));

                bool showError = true;
                if (ParentScrollRectSerialized.objectReferenceValue != null) {
                    if (TargetCasted.transform.IsChildOrSubChildOf(((ScrollRect)ParentScrollRectSerialized.objectReferenceValue).transform)) {
                        showError = false;
                    }
                }


                

                EditorGUILayout.HelpBox("Button is marked as a draggable button, ie : a button place in a Scroll View. A parent ScrollRect must be assigned to redirect Drag event from the button to the ScrollRect"
                    , showError ? MessageType.Error : MessageType.None);
            }
            else {
                EditorGUILayout.PropertyField(TriggerClickAtPointerUpSerialized, new GUIContent("Trigger OnClick() at PointerUP"),GUILayout.ExpandWidth(true));
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(OnClickSerialized, new GUIContent("On Click"), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(ComputeSubmitAsClickSerialized, new GUIContent("Compute \"Submit\" as \"Click\" ?"), GUILayout.ExpandWidth(true));
            if (ComputeSubmitAsClickSerialized.boolValue == false) {
                EditorGUILayout.PropertyField(OnSubmitSerialized, new GUIContent("On Submit"), GUILayout.ExpandWidth(true));
            } else {
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                EditorGUILayout.Space();
            }

            EditorGUILayout.PropertyField(OnStartHighlightSerialized, new GUIContent("On Start Highlitght"), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(OnStopHighlightSerialized, new GUIContent("On Stop Highlight"), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(OnSelectByNavigationSerialized, new GUIContent("On Select"), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(OnDeselectByNavigationSerialized, new GUIContent("On Deselect"), GUILayout.ExpandWidth(true));

            serializedObject.ApplyModifiedProperties();
        }


    }


#endif

}
