
## READ BEFORE USE

This C# script file is intended to be used for **UnityEngine** projects,  
it is created to be used as an alternative of the default **Button component**.  
Based on the Selectable script, it displays more **UnityEvent**s on the
inspector:  
OnClick  
OnSubmit  
OnStartHighlight  
OnStopHighlight  
OnSelectByNavigation  
OnDeselectByNavigation  

Usage examples:  
By using it toghether with a tailored script, it allow more precise display   
changes (but it is required to set transition to None), or to display animations
on the background when a button is highlithed.


